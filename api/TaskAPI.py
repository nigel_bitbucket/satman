from flask_restful import Resource
from flask import request
import logging as logger
import db
import json
import requests
import os


class Task(Resource):

    def get(self):
        token = request.headers['token']
        depl = os.environ['deploymentvariable']
        depl = depl.rstrip()

        base_url = "http://secman-service-{}:5002/api/v1.0/user/validate".format(depl)

        headers = {
            "token": token
        }

        response = requests.get(base_url, headers=headers)

        response = int(response.text.rstrip())

        if response == 1:
            rand_token = 'Token Validated!'
            sql = "select * from satellite;"
            db.cur.execute(sql)
            result = db.cur.fetchall()
            # logger.debug("Inside get method of Task result - {}".format(rand_token))
            # return result, 200
            return json.dumps(result), 200

        else:
            rand_token = 'Invalid Token!'
            logger.debug("Inside get method of Task result - {}".format(rand_token))
            return rand_token, 200


    def post(self):

        satid = request.json['id']
        alias = request.json['alias']
        description = request.json['description']
        status = request.json['status']
        locationid = request.json['locationid']
        launchsite = request.json['launchsite']
        owner = request.json['owner']

        sql = "insert into satellite values (%s,%s,%s,%s,%s,%s,%s)"
        db.cur.execute(sql, (satid, alias, description, status, locationid, launchsite, owner))

        db.conn.commit()
        logger.debug("Inisde the post method of Task")
        return {"message": "APPV7 Added New Satellite - {}".format(alias)}, 200
