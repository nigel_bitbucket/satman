from flask_restful import Resource
import logging as logger
import db
import json
from flask import request


# Get Satellite by IDb
class TaskByID(Resource):

    def get(self, satId):
        sql = "select * from satellite where id = %s"
        db.cur.execute(sql, satId, )

        result = db.cur.fetchall()
        # return the results!
        logger.debug("Inisde the get method of TaskById. TaskID = {}".format(satId))
        return json.dumps(result), 200


# Decommission Satellite
class TaskByID1(Resource):

    def get(self, satId):
        sql = "select * from satellite where id = %s"
        db.cur.execute(sql, satId, )

        result = db.cur.fetchall()
        # return the results!
        logger.debug("Inisde the get method of TaskById. TaskID = {}".format(satId))
        return json.dumps(result), 200

    def put(self, satId):
        sql = "update satellite set status = 0 where id = %s"
        db.cur.execute(sql, satId, )

        db.conn.commit()
        logger.debug("Inisde the put method of TaskByID. TaskID = {}".format(satId))
        return {"message": "Decommissioned Satellite, ID = {}".format(satId)}, 200


# Update Satellite Description
class TaskByID2(Resource):

    def get(self, satId):
        sql = "select * from satellite where id = %s"
        db.cur.execute(sql, satId, )

        result = db.cur.fetchall()
        # return the results!
        logger.debug("Inisde the get method of TaskById. TaskID = {}".format(satId))
        return json.dumps(result), 200

    def put(self, satId):
        description = request.json['description']

        sql = "update satellite set description = %s where id = %s"
        db.cur.execute(sql, (description, satId,))

        db.conn.commit()
        logger.debug("Inisde the put method of TaskByID. TaskID = {}".format(satId))
        return {"message": "Updated Description Field | Satellite, ID = {}. New Description = {}".format(satId,
                                                                                                         description)}, 200


# Update Satellite Alias
class TaskByID3(Resource):

    def get(self, satId):
        sql = "select * from satellite where id = %s"
        db.cur.execute(sql, satId, )

        result = db.cur.fetchall()

        # return the results!
        logger.debug("Inisde the get method of TaskById. TaskID = {}".format(satId))
        return json.dumps(result), 200

    def put(self, satId):
        alias = request.json['alias']

        sql = "update satellite set description = %s where id = %s"
        db.cur.execute(sql, (alias, satId,))
        db.conn.commit()
        logger.debug("Inisde the put method of TaskByID. TaskID = {}".format(satId))
        return {"message": "Updated Alias Field | Satellite, ID = {}. New Alias = {}".format(satId, alias)}, 200
